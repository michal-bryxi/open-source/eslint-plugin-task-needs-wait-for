import * as vitest from "vitest";
import { decoratorOrder } from "../rules/decorator-order.js";
import { RuleTester } from "@typescript-eslint/rule-tester";

RuleTester.afterAll = vitest.afterAll;
RuleTester.it = vitest.it;
RuleTester.itOnly = vitest.it.only;
RuleTester.describe = vitest.describe;

const ruleTester = new RuleTester({
  parser: "@typescript-eslint/parser",
});

ruleTester.run("task-needs-wait-for", decoratorOrder, {
  valid: [
    `class Foo { *example() { } }`,
    `class Foo { @task *example() { } }`,
    `class Foo { @waitFor *example() { } }`,
    `class Foo { @task @waitFor *example() { } }`,
    `class Foo { @task @waitFor *example1() { } @task @waitFor *example2() { } }`,
    `class Foo { @restartableTask @waitFor *example() { } }`,
    `class Foo { @dropTask @waitFor *example() { } }`,
    `class Foo { @keepLatestTask @waitFor *example() { } }`,
    `class Foo { @enqueueTask @waitFor *example() { } }`,
  ],
  invalid: [
    {
      code: `class Foo { @waitFor @task *example() { } }`,
      output: "class Foo { @task @waitFor *example() { } }",
      errors: [
        {messageId: 'task-before-wait-for'}
      ],
    },
    {
      code: `class Bar { @waitFor @task *example1() { } @task @waitFor *example2() { } }`,
      output: "class Bar { @task @waitFor *example1() { } @task @waitFor *example2() { } }",
      errors: [
        {messageId: 'task-before-wait-for'}
      ],
    },
  ],
});
