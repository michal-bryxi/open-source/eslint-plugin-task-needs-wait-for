import * as vitest from "vitest";
import { decoratorPresence } from "../rules/decorator-presence.js";
import { RuleTester } from "@typescript-eslint/rule-tester";

RuleTester.afterAll = vitest.afterAll;
RuleTester.it = vitest.it;
RuleTester.itOnly = vitest.it.only;
RuleTester.describe = vitest.describe;

const ruleTester = new RuleTester({
  parser: "@typescript-eslint/parser",
});

ruleTester.run("task-needs-wait-for", decoratorPresence, {
  valid: [
    `class Foo { *example() { } }`,
    `class Foo { @task @waitFor *example() { } }`,
    `class Foo { @restartableTask @waitFor *example() { } }`,
    `class Foo { @dropTask @waitFor *example() { } }`,
    `class Foo { @keepLatestTask @waitFor *example() { } }`,
    `class Foo { @enqueueTask @waitFor *example() { } }`,
  ],
  invalid: [
    {
      code: `class Foo { @task *example() { } }`,
      output: `import { waitFor } from '@ember/test-waiters';class Foo { @task @waitFor *example() { } }`,
      errors: [{ messageId: "task-without-wait-for" }],
    },
    {
      code: `class Foo { @restartableTask *example() { } }`,
      output: `import { waitFor } from '@ember/test-waiters';class Foo { @restartableTask @waitFor *example() { } }`,
      errors: [{ messageId: "task-without-wait-for" }],
    },
    {
      code: `class Foo { @dropTask *example() { } }`,
      output: `import { waitFor } from '@ember/test-waiters';class Foo { @dropTask @waitFor *example() { } }`,
      errors: [{ messageId: "task-without-wait-for" }],
    },
    {
      code: `class Foo { @keepLatestTask *example() { } }`,
      output: `import { waitFor } from '@ember/test-waiters';class Foo { @keepLatestTask @waitFor *example() { } }`,
      errors: [{ messageId: "task-without-wait-for" }],
    },
    {
      code: `class Foo { @enqueueTask *example() { } }`,
      output: `import { waitFor } from '@ember/test-waiters';class Foo { @enqueueTask @waitFor *example() { } }`,
      errors: [{ messageId: "task-without-wait-for" }],
    },
    {
      code: `import { waitFor } from '@ember/test-waiters'; class Foo { @task *example() { } }`,
      output: `import { waitFor } from '@ember/test-waiters'; class Foo { @task @waitFor *example() { } }`,
      errors: [{ messageId: "task-without-wait-for" }],
    },
  ],
});
