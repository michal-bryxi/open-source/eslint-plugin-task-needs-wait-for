import { decoratorPresence } from "./rules/decorator-presence.js";
import { decoratorOrder } from "./rules/decorator-order.js";

export const rules = {
  "decorator-presence": decoratorPresence,
  "decorator-order": decoratorOrder,
};

export const configs = {
  recommended: {
    plugins: ["task-needs-wait-for"],
    rules: {
      "task-needs-wait-for/decorator-presence": "error",
      "task-needs-wait-for/decorator-order": "error",
    },
  },
};
