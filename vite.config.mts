import { defineConfig, mergeConfig } from "vitest/config";
import viteConfig from "./vite.config.mjs";

// @ts-ignore
export default mergeConfig(
  viteConfig,
  defineConfig({
    test: {
      include: ["./tests/*-test.ts"],
    },
  }),
);
